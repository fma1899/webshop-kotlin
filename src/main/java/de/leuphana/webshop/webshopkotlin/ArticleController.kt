package de.leuphana.webshop.webshopkotlin

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ArticleController(val repository: ArticleRepository,val articleService: ArticleService) {

    @GetMapping("/article")
    fun findAll(): Iterable<Article> = repository.findAll()

    @PostMapping("/article/init/{num}")
    fun initArticle(@PathVariable num: Int){
        articleService.initArticle(num)
    }

    @GetMapping("/article/orderCount/{id}")
    fun getOrderCount(@PathVariable id: String): Int = articleService.getArticleCount(id)
}
