package de.leuphana.webshop.webshopkotlin

import org.junit.Assert.assertTrue

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
class WebshopKotlinApplicationTests {

    @Test
    fun contextLoads() {
        assertTrue("Context loaded", true)
    }

}
