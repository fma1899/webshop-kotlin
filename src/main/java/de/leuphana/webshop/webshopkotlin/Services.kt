package de.leuphana.webshop.webshopkotlin

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.redis.core.StringRedisTemplate
import org.springframework.data.redis.support.atomic.RedisAtomicInteger
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.security.SecureRandom
import java.util.*

@Service
class OrderService(val orderRepo: OrderRepository,
                   val customerRepo: CustomerRepository,
                   val articleRepo: ArticleRepository,
                   val articleService: ArticleService,
                   val addressRepo: AddressRepository) {

    @Transactional
    open fun orderBasket(basket: Basket, address: Address): Order {
        val articleMap = basket.items.map { Pair(it.articleNo, articleRepo.findOneByArticleNo(it.articleNo)) }.toMap()
        val customer = customerRepo.findOne(basket.customerId)
        val order = Order(orderNo = UUID.randomUUID().toString())

        order.items = basket.items.map {
            val article = articleMap[it.articleNo] ?: throw IllegalArgumentException("Article number ${it.articleNo} not found!")
            OrderItem(quantity = it.quantity, price = article.price, article = article)
        }

        order.totalAmount = order.items.map { it.price * it.quantity }.sum()
        order.address = address
        customer.addresses.add(address)
        customer.orders.add(order)
        addressRepo.save(address)
        orderRepo.save(order)
        customerRepo.save(customer)
        order.items.forEach { articleService.incOrderCount(it.article, it.quantity) }
        return order
    }
}

@Service
class ArticleService(val articleRepo: ArticleRepository) {

    @Autowired lateinit var redisTemplate: StringRedisTemplate

    val counterPrefix = "article:"

    val counterSuffix = ":counter"

    fun incOrderCount(article: Article, count: Int = 1): Int {
        val counter = RedisAtomicInteger(getCounterName(article.id), redisTemplate.connectionFactory)
        return counter.addAndGet(count)
    }

    fun getArticleCount(article: Article) = getArticleCount(article.id)

    fun getArticleCount(id: String) = RedisAtomicInteger(getCounterName(id), redisTemplate.connectionFactory).get()

    private fun getCounterName(articleId: String) = "$counterPrefix${articleId}$counterSuffix"

    fun initArticle(num: Int) = repeat(num, { i ->
        articleRepo.save(
                Article(
                        articleNo = (100000 + i).toString(),
                        price = (SecureRandom().nextInt(1000).toFloat() / 100),
                        name = "My Article with number $i",
                        description = "Some meaningful description")
        )
    })
}

@Service
class CustomerService(val customerRepo: CustomerRepository) {
    fun register(email: String, firstName: String, lastName: String): Customer? {
        if (customerRepo.findOneByEmail(email) != null) throw EmailAlreadyExistsException()
        return customerRepo.save(Customer(firstName = firstName, lastName = lastName, email = email))
    }

}
