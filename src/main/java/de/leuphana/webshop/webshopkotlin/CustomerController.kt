package de.leuphana.webshop.webshopkotlin

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
class CustomerController(val repository: CustomerRepository, var customerService: CustomerService) {

    @PostMapping("customer/register")
    fun register(@RequestParam email: String, @RequestParam firstName: String, @RequestParam lastName: String): Customer? =
            customerService.register(email, firstName, lastName)

    @PostMapping("customer/login")
    fun login(@RequestParam email: String) = repository.findOneByEmail(email)

    @GetMapping("/customer")
    fun findAll(): Iterable<Customer> = repository.findAll()

    @GetMapping("/customer/find/{email}")
    fun findByEmail(@PathVariable email: String) = repository.findOneByEmail(email)

    @PutMapping("/customer/save")
    fun saveCustomer(@RequestBody customer: Customer): Customer = repository.save(customer)

    @GetMapping("/customer/{id}/orders")
    fun getOrdersForCustomer(@PathVariable id: String) = repository.findOne(id)?.orders
}

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Email already exists")
class EmailAlreadyExistsException : RuntimeException()
