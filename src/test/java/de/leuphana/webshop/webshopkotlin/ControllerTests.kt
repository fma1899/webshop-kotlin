package de.leuphana.webshop.webshopkotlin

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.redis.core.StringRedisTemplate
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import kotlin.test.assertEquals
import kotlin.test.assertNotNull


class CustomerControllerUnitTest {

    lateinit var mockMvc: MockMvc

    @InjectMocks
    lateinit var controller: CustomerController

    @Mock
    lateinit var repository: CustomerRepository

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        mockMvc = MockMvcBuilders.standaloneSetup(controller).setMessageConverters(MappingJackson2HttpMessageConverter()).build()
    }

    @Test
    @Ignore
    fun findUser() {
        `when`(repository.findAll()).thenReturn(listOf(Customer(firstName = "Eric", lastName = "Cartman")))

        mockMvc.perform(MockMvcRequestBuilders.get("/customer")).andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    @Ignore
    fun registerCustomer() {
        val email = "eric.c@rtman.com"
        val firstName = "Eric"
        val lastName = "Cartman"

        `when`(repository.findOneByEmail(email)).thenReturn(null)

        mockMvc.perform(MockMvcRequestBuilders.post("/customer/register")
                .param("email", email)
                .param("firstName", firstName)
                .param("lastName", lastName))
                .andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    @Ignore
    fun registerCustomerFailsIfEmailIsKnown() {
        val email = "eric.c@rtman.com"
        val firstName = "Eric"
        val lastName = "Cartman"

        `when`(repository.findOneByEmail(email)).thenReturn(Customer(firstName = firstName, lastName = lastName, email = email))

        mockMvc.perform(MockMvcRequestBuilders.post("/customer/register")
                .param("email", email)
                .param("firstName", firstName)
                .param("lastName", lastName))
                .andExpect(status().isBadRequest)
    }
}

@RunWith(SpringJUnit4ClassRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class ControllerIntegrationTest {

    @Autowired lateinit var customerRepository: CustomerRepository

    @Autowired lateinit var articleRepository: ArticleRepository

    @Autowired lateinit var articleService: ArticleService

    @Autowired lateinit var redisTemplate: StringRedisTemplate

    @Autowired lateinit var mockMvc: MockMvc

    @Before
    fun setup() {
        redisTemplate.connectionFactory.connection.flushAll()
        articleService.initArticle(10)
    }

    private val testMail = "eric.cartman@jimbo.com"

    private val firstName = "Eric"

    private val lastName = "Cartman"

    @Test
    fun registerCustomer() {
        this.mockMvc.perform(post("/customer/register")
                .param("email", testMail)
                .param("firstName", firstName)
                .param("lastName", lastName))
                .andDo(print())
                .andExpect(status().isOk)
    }

    @Test
    fun loginCustomer() {
        val customer = customerRepository.save(Customer(firstName = firstName, lastName = lastName, email = testMail))
        this.mockMvc.perform(post("/customer/login")
                .param("email", customer.email))
                .andDo(print())
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.email").value(customer.email))
    }

    @Test
    fun webshopWorkflow() {
        val address = Address(street = "Elm Street 23", city = "South Park", zipCode = "235")
        val customer = Customer(firstName = firstName,
                lastName = lastName,
                email = testMail,
                addresses = mutableSetOf(address))

        // register
        this.mockMvc.perform(post("/customer/register")
                .param("email", testMail)
                .param("firstName", firstName)
                .param("lastName", lastName))
                .andDo(print())
                .andExpect(status().isOk)
        val persistedCustomer = customerRepository.findOneByEmail(testMail)
        assertNotNull(persistedCustomer)

        // login
        this.mockMvc.perform(post("/customer/login")
                .param("email", customer.email))
                .andDo(print())
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.email").value(customer.email))

        // buy
        val articles = articleRepository.findAll()
        val articleCount = 1
        val basket = Basket(customerId = persistedCustomer!!.id, items = articles.map { BasketItem(quantity = articleCount, articleNo = it.articleNo) })
        val request = OrderRequest(basket, address)
        this.mockMvc.perform(post("/order/buy")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapper().writeValueAsString(request)))
                .andExpect(status().isOk)
        val orderCustomer = customerRepository.findOneByEmail(testMail)
        assertNotNull(orderCustomer)
        assertEquals(orderCustomer?.orders?.get(0)?.items?.size, articles.count())

        // check ordered count
        val result = this.mockMvc.perform(get("/article/orderCount/${articles.first().id}"))
                .andExpect { status().isOk }
                .andReturn()
        assertEquals(result.response.contentAsString.toInt(), articleCount)
    }

}
