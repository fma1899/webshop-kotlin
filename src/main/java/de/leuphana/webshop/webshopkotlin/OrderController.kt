package de.leuphana.webshop.webshopkotlin

import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class OrderController(val orderService: OrderService) {

    @PostMapping("/order/buy")
    fun buyOrder(@RequestBody orderRequest: OrderRequest) = orderService.orderBasket(orderRequest.basket, orderRequest.shippingAddress)
}

data class OrderRequest(val basket:Basket, val shippingAddress: Address)
