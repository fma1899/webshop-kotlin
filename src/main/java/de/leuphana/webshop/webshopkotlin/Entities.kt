package de.leuphana.webshop.webshopkotlin

import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Reference
import org.springframework.data.redis.core.RedisHash
import org.springframework.data.redis.core.index.Indexed
import java.util.*

@RedisHash("customer")
data class Customer(
        @Id var id: String = UUID.randomUUID().toString(),
        var firstName: String = "",
        var lastName: String = "",
        @Indexed var email: String = "",
        var password: String = "",
        @Reference var orders: MutableList<Order> = mutableListOf(),
        @Reference var addresses: MutableSet<Address> = mutableSetOf()
)

@RedisHash("address")
data class Address(@Id var id: String = UUID.randomUUID().toString(),
                   var street: String = "",
                   var city: String = "",
                   var zipCode: String = "")

@RedisHash("order")
data class Order(@Id var id: String = UUID.randomUUID().toString(),
                 @Indexed var orderNo: String = "",
                 var totalAmount: Float = 0f,
                 var items: List<OrderItem> = mutableListOf(),
                 var address: Address? = null)

@RedisHash("orderItem")
data class OrderItem(@Id var id: String = UUID.randomUUID().toString(),
                     var quantity: Int = -1,
                     var price: Float = -1f,
                     @Reference var article: Article = Article())

@RedisHash("article")
data class Article(@Id var id: String = UUID.randomUUID().toString(),
                   @Indexed var articleNo: String = "",
                   var price: Float = -1f,
                   var name: String = "",
                   var description: String = "")

data class Basket(var items: List<BasketItem>, var customerId: String)

data class BasketItem(var quantity: Int, var articleNo: String)
