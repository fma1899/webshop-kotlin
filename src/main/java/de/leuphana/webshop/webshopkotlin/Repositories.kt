package de.leuphana.webshop.webshopkotlin

import org.springframework.data.repository.CrudRepository

interface CustomerRepository : CrudRepository<Customer, String> {
    fun findOneByEmail(email: String): Customer?
}

interface OrderRepository : CrudRepository<Order, String>

interface ArticleRepository : CrudRepository<Article, String> {
    fun findOneByArticleNo(articleNo: String): Article?
}

interface OrderItemRepository : CrudRepository<OrderItem, String>

interface AddressRepository : CrudRepository<Address, String>
