package de.leuphana.webshop.webshopkotlin

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.redis.core.StringRedisTemplate
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@RunWith(SpringJUnit4ClassRunner::class)
@SpringBootTest
class WorkflowIntegrationTest {
    @Autowired lateinit var orderService: OrderService

    @Autowired lateinit var customerRepository: CustomerRepository

    @Autowired lateinit var customerService: CustomerService

    @Autowired lateinit var articleRepository: ArticleRepository

    @Autowired lateinit var articleService: ArticleService

    @Autowired lateinit var redisTemplate: StringRedisTemplate

    @Before
    fun setup() {
        redisTemplate.connectionFactory.connection.flushAll()
    }

    @Test
    fun workflowTest() {
        val email = "eric.cartman@jimbos.com"
        val firstName = "Eric"
        val lastName = "Cartman"
        val address = Address(street = "Elm Street 23", city = "South Park", zipCode = "235")

        articleService.initArticle(10)
        val articles = articleRepository.findAll()
        assertEquals(articles.toList().size, 10)

        val customer = customerService.register(email, firstName, lastName)
        assertNotNull(customerRepository.findOneByEmail(email))

        val basket = Basket(customerId = customer!!.id, items = articles.map { BasketItem(quantity = 1, articleNo = it.articleNo) })

        val order = orderService.orderBasket(basket, address)

        assertEquals(customerRepository.findOneByEmail(email)?.orders?.size, 1)
        assertEquals(order.items.size, 10)

        order.items.forEach { assertEquals(articleService.getArticleCount(it.article), 1) }
    }
}
