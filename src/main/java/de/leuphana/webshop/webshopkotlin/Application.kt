package de.leuphana.webshop.webshopkotlin

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory
import org.springframework.data.redis.core.StringRedisTemplate
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories

@SpringBootApplication
open class Application

fun main(args: Array<String>) {
    SpringApplication.run(Application::class.java, *args)
}

@Configuration
@EnableRedisRepositories
open class Config{
    @Bean
    open fun jedisConnectionFactory(): JedisConnectionFactory {
        val connectionFactory = JedisConnectionFactory()
        connectionFactory.hostName = "localhost"
        connectionFactory.port = 6379
        return connectionFactory
    }

    @Bean
    open fun redisTemplate(): StringRedisTemplate {
        val template = StringRedisTemplate()
        template.connectionFactory = jedisConnectionFactory()
        template.setEnableTransactionSupport(true)
        return template
    }
}
