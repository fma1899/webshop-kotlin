#!/usr/bash
# save customer
curl -XPUT -H "Content-Type: application/json" -d @saveCustomer.json http://localhost:8080/customer/save
# get all customers
curl http://localhost:8080/customer
# find all by lastName
curl http://localhost:8080/customer/find/eric.c@rtman.com
# init insert articles
curl -XPOST -H "Content-Type: application/json" -d @saveCustomer.json http://localhost:8080/article/init/5
# find all articles
curl http://localhost:8080/article | python -m json.tool
# create order
curl -XPOST -H "Content-Type: application/json" -d @saveOrder.json http://localhost:8080/order/buy 
# get orders for user
curl http://localhost:8080/customer/d852fca4-1218-4b12-9801-31ff6f9e65a5/orders | python -m json.tool
